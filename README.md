# Word Censor

## Introduction

The Word Censor module provides a way to censor vulgar (bad) words by replacing
the bad word with a character such as *. 
Currently the module only provides a service which can used to pass in text and
using the default dictionary or 
a custom dictionary any "bad" word found in the text will replaced with
asterisks.

Example, "This is shitty" would become "This is ****ty".

## Requirements

This module depends on this third party library here 
[Snipe/banbuilder](https://github.com/snipe/banbuilder) which is included in
the composer.json and no further action is needed when installing this module
with composer.

## Installation

Recommended way to install is with composer. Visit https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#managing-contributed 
for more information on using composer with Drupal.

## Configuration

While there already is a default dictionary of bad words there is a config
page (/admin/config/system/wordcensor) to set a path to a custom dictionary
relative to the Drupal install root directory.

To create a custom dictionary see the "Creating and Using Your Own Dictionaries"
section here https://banbuilder.com/.

## Troubleshooting

## FAQ

## Maintainers

Current Maintainers:

* Jon Kamke (kamkejj) - https://www.drupal.org/u/kamkejj
* Dan Flanagan (danflanagan8) - https://www.drupal.org/u/danflanagan8
