<?php

namespace Drupal\Tests\word_censor\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the module settings page.
 *
 * @group word_censor
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  public static $modules = [
    'user',
    'word_censor',
  ];

  /**
   * Tests the setting form.
   */
  public function testForm() {
    // Create the user with the appropriate permission.
    $admin_user = $this->drupalCreateUser(
      [],
      'testadmin',
      TRUE
    );

    // Start the session.
    $session = $this->assertSession();

    // Login as our account.
    $this->drupalLogin($admin_user);

    // Get the settings form path from the route.
    $settings_form_path = Url::fromRoute('word_censor.word_censor_config_form');

    // Navigate to the settings form.
    $this->drupalGet($settings_form_path);

    // Assure we loaded settings with proper permissions.
    $session->statusCodeEquals(200);

    // Test dictionary_path field.
    $dictionaryPathFieldValue = 'modules/contrib/word_censor/tests/modules/word_censor_tests/files/customDictionary.php';
    $edit = [
      'dictionary_path' => $dictionaryPathFieldValue,
    ];
    // Update our text field with a new value.
    $this->drupalPostForm($settings_form_path, $edit, 'Save configuration');

    // Reload the page.
    $this->drupalGet($settings_form_path);

    $my_text_field = $session->fieldExists('dictionary_path')->getValue();

    // Check that the field value matches.
    $this->assertTrue($my_text_field === $dictionaryPathFieldValue);
  }

}
