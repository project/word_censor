<?php

namespace Drupal\Tests\word_censor\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Class for testing the Word Censor Service.
 *
 * @group word_censor
 */
class WordCensorServiceTests extends KernelTestBase {

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  public static $modules = ['word_censor', 'word_censor_tests'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig('word_censor');
  }

  /**
   * Test the default dictionary.
   */
  public function testDefaultDictionary() {
    $wordCensorService = \Drupal::service('word_censor.service');
    // This is the least vulgar test I could come up with!
    $vulgar = "Joe Cocker is great and Octopussy stars Roger Moore.";
    $clean = $wordCensorService->cleanString($vulgar);
    $this->assertEqual($clean, "Joe ****er is great and Octo***** stars Roger Moore.");
  }

  /**
   * Test a custom dictionary.
   */
  public function testCustomDictionary() {
    $config = \Drupal::service('config.factory')->getEditable('word_censor.settings');
    $config->set('dictionary_path', __DIR__ . "/../../modules/word_censor_tests/files/customDictionary.php");
    $config->save();
    $wordCensorService = \Drupal::service('word_censor.service');
    $vulgar = "In Harry Potter most people do no say Voldemort.";
    $clean = $wordCensorService->cleanString($vulgar);
    $this->assertEqual($clean, "In Harry Potter most people do no say *********.");
  }

}
