<?php

namespace Drupal\word_censor\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Snipe\BanBuilder\CensorWords;

/**
 * Class WordCensorService.
 */
class WordCensorService implements WordCensorServiceInterface {

  /**
   * The cron configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Drupal\Core\Plugin\Context\ContextProviderInterface definition.
   *
   * @var \Drupal\Core\Plugin\Context\ContextProviderInterface
   */
  protected $languageCurrentLanguageContext;

  /**
   * The CensorWords class.
   *
   * @var \Snipe\BanBuilder\CensorWords
   */
  private $censorWords;

  /**
   * Constructs a new WordCensorService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Plugin\Context\ContextProviderInterface $language_current_language_context
   *   The language context service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ContextProviderInterface $language_current_language_context) {
    $this->config = $config_factory->get('word_censor.settings');
    $this->languageCurrentLanguageContext = $language_current_language_context;
    $this->censorWords = $this->getCensorWords();
    $this->setDictionaryFromConfig();
  }

  /**
   * Check word(s) against the banned dictionary.
   *
   * One or more words to check against the ban dictionary.
   *
   * @param string $string
   *   The string to apply the censor rules.
   *
   * @return string
   *   The cleaned version of the string.
   */
  public function cleanString($string) {
    $clean = $this->censorWords->censorString($string);

    return $clean['clean'];
  }

  /**
   * Set the language.
   *
   * One or more languages to test against.
   *
   * @param array $languages
   *   Array of language codes to use.
   *
   * @return \Drupal\word_censor\Service\WordCensorService
   *   WordCensorService.
   */
  public function setLanguage(array $languages) {
    if (empty($languages)) {
      return $this;
    }

    $this->censorWords->setDictionary($languages);

    return $this;
  }

  /**
   * The character to replace the banned word with, defaults to asterisks.
   *
   * Set to a single character to use as the text replacement for the
   * banned word.
   *
   * @param string $char
   *   Character to use as the replacement.
   *
   * @return \Drupal\word_censor\Service\WordCensorService
   *   WordCensorService.
   */
  public function setReplaceChar($char) {
    if (strlen($char) === 1) {
      $this->censorWords->setReplaceChar($char);
    }

    return $this;
  }

  /**
   * Set a custom dictionary from configuration.
   *
   * If the configuration has a valid path set then set that as the
   * custom dictionary.
   */
  private function setDictionaryFromConfig() {
    $dictionaryPath = $this->config->get('dictionary_path');

    if (empty($dictionaryPath)) {
      return;
    }

    $this->censorWords->setDictionary($dictionaryPath);
  }

  /**
   * Get the CensorWords class.
   *
   * @return \Snipe\BanBuilder\CensorWords
   *   CensorWords.
   */
  private function getCensorWords() {
    return new CensorWords();
  }

}
